@color @core @api
Feature: Color functionality
  In order change the colour of my site
  As an admin user
  I want to be able to edit the theme colours

  Background:
    Given I am logged in as a user with "administer themes" permissions
    When I go to "admin/appearance/settings/bartik"

  Scenario: View Appearance settings page
    Then I should see the link "Global settings"
    And I should see the link "Seven"
    And I should see "Color Scheme"
    And I should see "Toggle Display"
    And I should see "Logo image settings"
    And I should see "Shortcut icon settings"

  Scenario Outline: Invalid color selection
    When I fill in "edit-palette-bg" with "<colors>"
    And I press "Save configuration"
    Then I should see the error message "You must enter a valid hexadecimal color value for Main background."
  Examples:
    | colors  |
    | #00     |
    | #0000   |
    | #00000  |
    | 123456  |
    | #00000g |

  Scenario Outline: Valid color selection
    When I fill in "edit-palette-bg" with "<colors>"
    And I press "Save configuration"
    Then I should see the message "The configuration options have been saved."
  Examples:
    | colors  |
    | #000    |
    | #123456 |
    | #abcdef |


