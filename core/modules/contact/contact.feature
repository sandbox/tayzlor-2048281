@contact @core @api
Feature: Contact form
  In order to submit a query about a site
  As an web user
  I want to be able to submit a contact form.

  Scenario: Verify contact form textfields do not appear for a logged in user
    Given I am logged in as a user with the "authenticated user" role
    And I go to "/contact"
    Then I should not see a "name" element
    And I should not see a "mail" element

  Scenario: Administer contact form page
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact"
    Then I should see the text "Personal contact form"
    And I should see the text "Website feedback"

  Scenario Outline: Test invalid email address on add category form
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact/add"
    And I fill in "label" with random text
    And I fill in "id" with random text
    And I fill in "recipients" with "<emails>"
    And I press "Save"
    Then I should see the error message "<emails> is an invalid e-mail address."
    And the field "recipients" should be outlined in red
  Examples:
    | emails        |
    | invalid       |
    | invalid@      |
    | invalid@site. |

  Scenario: Test empty category form submission
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact/add"
    And I press "Save"
    Then I should see the following <error messages>
      | error messages                           |
      | Label field is required.                 |
      | Machine-readable name field is required. |
      | Recipients field is required.            |

  Scenario: Create a valid category
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact/add"
    And I fill in "label" with "abcde"
    And I fill in "id" with "abcde"
    And I fill in "recipients" with "simpletest@example.com"
    And I press "Save"
    Then I should see the success message "Category abcde has been added."

  Scenario: Test contact form category edit form
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact/manage/abcde"
    Then the "label" field should contain "abcde"
    And the "id" field should contain "abcde"
    And the "recipients" field should contain "simpletest@example.com"

  Scenario: Test contact form category update
    Given I am logged in as "admin user"
    And I go to "/admin/structure/contact/manage/abcde"
    And I fill in "reply" with "auto reply text"
    And I press "Save"
    Then I should see the success message "Category abcde has been updated."

  Scenario: Delete contact form category
    Given I am logged in as "admin user"
    And I go to "admin/structure/contact/manage/abcde/delete"
    And I press "Delete"
    Then I should see the success message "Category abcde has been deleted."



