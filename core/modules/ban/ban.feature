@ban @core @api
Feature: IP address banning
  In order stop somebody accessing my site
  As an admin user
  I should be able to ban certain IP addresses

  Background:
    Given I am logged in as a user with "ban IP addresses" permissions
    When I go to "admin/config/people/ban"

  Scenario: Ban a valid IP address
    When I fill in "ip" with "192.168.1.1"
    And I press "Add"
    Then I should see the success message "The IP address 192.168.1.1 has been banned."

  Scenario: Try to block an IP address that's already blocked
    When I fill in "ip" with "192.168.1.1"
    And I press "Add"
    Then I should see the error message "This IP address is already banned."
    And the field "ip" should be outlined in red

  Scenario Outline: Try to block a reserved IP address
    When I fill in "ip" with "<ips>"
    And I press "Add"
    Then I should see the error message "Enter a valid IP address."
    And the field "ip" should be outlined in red
  Examples:
    | ips              |
    | 255.255.255.255  |
    | test.example.com |

  Scenario: Submit an empty form
    When I press "Add"
    Then I should see the error message "Enter a valid IP address."
    And the field "ip" should be outlined in red

  Scenario: Delete banned IP address
    When I follow "delete"
    And I press "Delete"
    Then I should see the success message "The IP address 192.168.1.1 was deleted."

  Scenario: Pass an IP address as a URL parameter and submit it
    When I go to "admin/config/people/ban/1.2.3.4"
    And I press "Add"
    Then I should see the message "The IP address 1.2.3.4 has been banned."


